<?php



/**
 * Implementation of hook_views_default_views()
 */
function uc_pdf_invoice_views_default_views() {
  $views = array();
  $path = drupal_get_path('module', 'uc_pdf_invoice');


  // Import the orders view
  include $path . '/uc_pdf_invoice_default_views_orders.inc';
  $views[$view->name] = $view;

  // Import the invoice view
  include $path . '/uc_pdf_invoice_default_views_invoice.inc';
  $views[$view->name] = $view;

  // Import the invoice lines view
  include $path . '/uc_pdf_invoice_default_views_invoice_lines.inc';
  $views[$view->name] = $view;

  return $views;
}
